# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170209013016) do

  create_table "branches", primary_key: "brache_id", force: true do |t|
    t.integer "enterprise_id",             null: false
    t.string  "nombre",        limit: 100, null: false
    t.string  "calle",         limit: 100, null: false
    t.string  "colonia",       limit: 100, null: false
    t.integer "numExt",                    null: false
    t.integer "numInt",                    null: false
    t.integer "cp",                        null: false
    t.string  "ciudad",        limit: 100, null: false
    t.string  "pais",          limit: 100, null: false
  end

  add_index "branches", ["brache_id"], name: "brache_id_UNIQUE", unique: true, using: :btree
  add_index "branches", ["enterprise_id"], name: "enterprise_id_idx", using: :btree

  create_table "employees", primary_key: "employee_id", force: true do |t|
    t.integer "branche_id",             null: false
    t.string  "rfc",        limit: 13,  null: false
    t.string  "nombre",     limit: 100, null: false
    t.string  "puesto",     limit: 100, null: false
  end

  add_index "employees", ["branche_id"], name: "nombre_idx", using: :btree
  add_index "employees", ["employee_id"], name: "employee_id_UNIQUE", unique: true, using: :btree

  create_table "enterprises", primary_key: "enterprise_id", force: true do |t|
    t.string "nombre",    limit: 100, null: false
    t.string "apellidos", limit: 100, null: false
    t.string "email",     limit: 45,  null: false
    t.text   "password",              null: false
  end

  add_index "enterprises", ["enterprise_id"], name: "enterprise_id_UNIQUE", unique: true, using: :btree

  create_table "users", force: true do |t|
    t.string   "email"
    t.string   "password_hash"
    t.string   "password_salt"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
